import http from './http-common';

export class paymentService {

    getServiceStatus() {
        return http.get("/status");
    }

    getAll() {
        return http.get('/all');
    }

    save(payment) {
        return http.post('/', payment);
    }
}

export default new paymentService();