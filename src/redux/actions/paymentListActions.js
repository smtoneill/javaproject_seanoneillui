import { PAYMENT_LIST_REQUEST, PAYMENT_LIST_SUCCESS, PAYMENT_LIST_FAIL } from "../constants";
import paymentService from "../../services/paymentService";
import customers from "../../data/customers";

export function paymentListRequest() {
    return { type: PAYMENT_LIST_REQUEST }
};

export function paymentListSuccess(payments) {
    return { type: PAYMENT_LIST_SUCCESS, payments }
};

export function paymentListFail(error) {
    return { type: PAYMENT_LIST_FAIL, error }
}

// Note: Normally the customer info would come back from the REST API, but since this currently does not exist in the
// API, we are mapping this manually from an array of hard-coded values so that the UI displays the customer name and
// not an anonymous ID value. A future version would add the customer data to the REST service API.

const mapPaymentCustomer = (payments, customers) => {
    return payments.map(p => {
        return { ...p, customer : customers.find(c => c.id === p.custId) }
    });
}

export function loadPayments() {
    return function (dispatch) {
        dispatch(paymentListRequest());

        paymentService.getAll()
            .then(response => {
                let mapped = mapPaymentCustomer(response.data, customers);
                dispatch(paymentListSuccess(mapped));
            })
            .catch(error => {
                dispatch(paymentListFail(error.message));
                throw error;
            });
    }
}