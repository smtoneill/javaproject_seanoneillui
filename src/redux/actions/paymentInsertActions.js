import { PAYMENT_INSERT_REQUEST, PAYMENT_INSERT_SUCCESS, PAYMENT_INSERT_FAIL } from "../constants";
import paymentService from "../../services/paymentService";
import customers from "../../data/customers";

export function paymentInsertRequest() {
    return { type: PAYMENT_INSERT_REQUEST };
};

export function paymentInsertSuccess(payment) {
    return { type: PAYMENT_INSERT_SUCCESS, payment };
};

export function paymentInsertFail(error) {
    return { type: PAYMENT_INSERT_FAIL, error };
}

export function insertPayment(payment) {
    return function (dispatch) {
        dispatch(paymentInsertRequest());

        return paymentService.save(payment)
            .then(response => {
                // Append the ID of the inserted record that is returned by the server and dispatch the success action.
                let newId = response.data;
                let newPayment = { ...payment, id: newId, customer: customers.find(c => c.id === payment.custId)}
                dispatch(paymentInsertSuccess(newPayment));
            })
            .catch(error => {
                dispatch(paymentInsertFail(error.message));
                throw error;
            });
    }
}