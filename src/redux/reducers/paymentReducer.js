import * as actionTypes from "../constants";
import initialState from "../initialState";

export default function paymentReducer(state = initialState, action) {

    switch (action.type) {
        case actionTypes.PAYMENT_LIST_REQUEST:
        case actionTypes.PAYMENT_INSERT_REQUEST:
            return { ...state, pending: true, error: null };
        case actionTypes.PAYMENT_LIST_SUCCESS:
            return { ...state, pending: false, payments: action.payments, error: null };
        case actionTypes.PAYMENT_INSERT_SUCCESS:
            return { ...state, pending: false, payments: [...state.payments, action.payment], error: null };
        case actionTypes.PAYMENT_LIST_FAIL:
        case actionTypes.PAYMENT_INSERT_FAIL:
            return { ...state, pending: false, error: action.error };
        default:
            return state;
    }
}