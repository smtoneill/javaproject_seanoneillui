import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import paymentReducer from "./reducers/paymentReducer"
import initialState from "./initialState";

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;

const store = createStore(
    paymentReducer,
    initialState,
    //  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    composeEnhancer(applyMiddleware(thunk))
);

export default store;
