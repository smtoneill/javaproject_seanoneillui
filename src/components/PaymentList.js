import React from "react";
import {Link} from "react-router-dom";
import { formatCurrency, formatDate } from "../common/formatters";

const PaymentList = ({ payments }) => {

    return (
        <table className="table table-striped">
            <thead className=" thead-dark">
            <tr>
                <th>Date/Time</th>
                <th>Type</th>
                <th>Amount</th>
                <th>Customer</th>
                <th/>
            </tr>
            </thead>
            <tbody>
            {payments.length > 0 ? payments.map(payment => (
                    <tr key={payment.id}>
                        <td style={{width: "15rem"}}>{formatDate(payment.paymentDate)}</td>
                        <td style={{width: "10rem"}}>{payment.type}</td>
                        <td style={{width: "10rem"}}>{formatCurrency(payment.amount)}</td>
                        <td style={{width: "20rem"}}>{payment.customer ? payment.customer.name : payment.custId}</td>
                        <td className="text-right">
                            <Link to={"/view/" + payment.id} className="btn btn-success">Details</Link>
                        </td>
                    </tr>
                )) : (
                <tr className="table-info">
                    <td colSpan="5" style={{ textAlign: "center" }}>
                        There are no payments to display.
                    </td>
                </tr>
            )}
            </tbody>
        </table>
    );
};

export default PaymentList;