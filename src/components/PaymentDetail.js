import React from "react";
import {connect} from "react-redux";
import { formatCurrency, formatDate } from "../common/formatters";

function PaymentDetail({payment}) {

    return (
        <>
            <h2>Payment Details</h2>

            {payment ? (
                <div className="mx-3 my-4">
                    <div className="row bg-light p-2 border-top border-bottom border-secondary">
                        <div className="col-lg-2 font-weight-bold">ID</div>
                        <div className="col-lg-10">{payment.id}</div>
                    </div>
                    <div className="row p-2 border-bottom border-secondary">
                        <div className="col-lg-2 font-weight-bold">Date</div>
                        <div className="col-lg-10">{formatDate(payment.paymentDate)}</div>
                    </div>
                    <div className="row bg-light p-2 border-bottom border-secondary">
                        <div className="col-lg-2 font-weight-bold">Type</div>
                        <div className="col-lg-10">{payment.type}</div>
                    </div>
                    <div className="row p-2 border-bottom border-secondary">
                        <div className="col-lg-2 font-weight-bold">Amount</div>
                        <div className="col-lg-10">{formatCurrency(payment.amount)}</div>
                    </div>
                    <div className="row bg-light p-2 border-bottom border-secondary">
                        <div className="col-lg-2 font-weight-bold">Customer</div>
                        <div className="col-lg-10">{payment.customer ? payment.customer.name : payment.custId}</div>
                    </div>

                </div>
            ) : (
                <div className="alert alert-danger my-3">
                    The specified payment was not found!
                </div>
            )}
        </>
    );
}

// The PaymentDetail page displays the details of a payment that has already been loaded
// into the Redux store. The payment id to display is specified by the URL "id" parameter.

function findPayment(payments, id) {

    if(payments.length > 0 && id > 0) {
        return payments.find(p => p.id === id);
    }
    else {
        return null;
    }
}

const mapStateToProps = (state, ownProps) => ({
    payment: findPayment(state.payments, Number.parseInt(ownProps.match.params.id))
});

export default connect(mapStateToProps)(PaymentDetail);
