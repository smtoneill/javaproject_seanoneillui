import React, {useEffect, useState} from "react";
import PaymentService from "../services/paymentService";

function About(props) {

    const [apiStatus, setStatus] = useState({ statusCode: 'fetching', message: "Please Wait. Retrieving Status..." });

    useEffect(() => {

        PaymentService.getServiceStatus()
            .then(response => {
                setStatus({ statusCode: 'success', message: response.data});
            })
            .catch(error => {
                // axios throws an error when the response from the server is not in the 2xx range or when no response
                // is received.
                let message = error.message ? error.message : "An unknown error occurred.";

                setStatus({
                    statusCode: 'fail',
                    message: "An error has occurred: " + message
                });
            });

    }, []);

    return (
        <div className="my-3">
            <h2>About</h2>

            <div className="card mt-4">
                <div className={
                    "card-header text-white " + (
                        apiStatus.statusCode === "success" ? "bg-success" :
                        apiStatus.statusCode === "fail" ? "bg-danger" :
                        apiStatus.statusCode === "fetching" ? "bg-info" : ""
                    )
                }>
                    API Service Status
                </div>
                <div className="card-body">
                    <p className="card-text">{apiStatus.message}</p>
                </div>
            </div>
        </div>
    );
}

export default About;
