import React from "react";
import {connect} from "react-redux";
import PaymentList from "./PaymentList";
import { loadPayments } from "../redux/actions/paymentListActions"
import { Link } from "react-router-dom";

class PaymentListPage extends React.Component {

    constructor(props) {
        super(props);

        this.state = { typeFilter : "" }
        this.handleFilterChange = this.handleFilterChange.bind(this);
    }

    componentDidMount() {
        const { payments, loadPayments } = this.props;

        if(payments.length === 0){
            loadPayments();
        }
    }

    handleFilterChange(event) {
        this.setState({typeFilter: event.target.value});
    }

    render() {

        const { payments, pending, error } = this.props;

        return (
            <>
                <h2>View All Payments</h2>

                {error ? (
                    <div className="alert alert-danger my-5">
                        An error occurred when retrieving payments. The error message was: {error}
                    </div> ) : (
                pending ? (
                    <div className="alert alert-warning my-5 d-flex align-items-center">
                        <div className="spinner-border text mr-3" role="status"></div> Loading. Please wait...
                    </div> ) : (
                    <>
                        <div className="mt-5 mb-3 form-row justify-content-between">
                            <div className="col-auto">
                                <div className="input-group">
                                    <div className="input-group-prepend">
                                        <span className="input-group-text">Filter By Type</span>
                                    </div>
                                    <select className="form-control" value={this.state.typeFilter} onChange={this.handleFilterChange}>
                                        <option value="">All</option>
                                        <option value="cash">Cash</option>
                                        <option value="card">Card</option>
                                        <option value="cheque">Cheque</option>
                                        <option value="transfer">Bank Transfer</option>
                                    </select>
                                </div>
                            </div>
                            <div className="col-auto">
                                <Link to="/add" className="btn btn-primary">Add New Payment</Link>
                            </div>
                        </div>
                        <PaymentList payments={getFilteredPayments(payments, this.state.typeFilter)} />
                    </>
                ))}
            </>
        );
    }
}

const getFilteredPayments = (payments, type) => {

    if(type && payments.length > 0) {
        return payments.filter(p => p.type === type);
    }
    else {
        return payments;
    }
}



const mapStateToProps = state => ({
    payments: state.payments,
    pending: state.pending,
    error: state.error
});

const mapDispatchToProps = dispatch => {
    return {
        loadPayments: () => dispatch(loadPayments())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(PaymentListPage);