import React from "react";
import customers from "../data/customers";
import { insertPayment } from "../redux/actions/paymentInsertActions";
import {connect} from "react-redux";

class AddPayment extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            paymentDate: "",
            type: "",
            amount: "",
            custId: "0",
            formErrors: []
        };

        this.handleChange = this.handleChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.checkPaymentIsValid = this.checkPaymentIsValid.bind(this);
    }

    handleChange(event) {
        const { name, value } = event.target;
        this.setState({ [name]: value });
    }

    onSubmit(event) {
        event.preventDefault();

        const newPayment = {
            id: 0,
            paymentDate: this.state.paymentDate,
            type: this.state.type,
            amount: Number.parseFloat(this.state.amount),
            custId: Number.parseInt(this.state.custId)
        };

        const formErrors = this.checkPaymentIsValid(newPayment);

        this.setState({ formErrors });

        if(formErrors.length === 0){
            this.props.insertPayment(newPayment)
                .then(() => {
                    // Force a redirect to the payment list when the payment has been saved.
                    this.props.history.push('/list')
                })
        }
    }

    checkPaymentIsValid(values) {

        let errors = [];

        if(!values.paymentDate) {
            errors.push("paymentDate");
        }

        if(!values.type) {
            errors.push("type");
        }

        if(!values.amount || values.amount.isNaN || values.amount <= 0) {
            errors.push("amount");
        }

        if(!values.custId || values.custId.isNaN || values.custId <= 0) {
            errors.push("custId");
        }

        return errors;
    }

    getFormControlStyles(name) {
        let classNames = "form-control ";

        if(this.state.formErrors.includes(name)) {
            classNames += "is-invalid";
        }

        return classNames;
    }

    render() {

        return (
            <>
                <h2>Add New Payment</h2>
                {this.state.formErrors.length > 0 &&
                    <div className="alert alert-danger my-3">
                        One or more errors have been found. Please correct and try again.
                    </div>
                }
                {this.props.error &&
                    <div className="alert alert-danger my-3">
                        An error occurred while saving the payment. The error message was: {this.props.error}
                    </div>
                }
                <form className="my-4">
                    <div className="form-group">
                        <label htmlFor="paymentDate">Payment Date</label>
                        <input type="datetime-local"
                               id="paymentDate"
                               name="paymentDate"
                               value={this.state.paymentDate}
                               onChange={this.handleChange}
                               className={this.getFormControlStyles("paymentDate")}
                               aria-describedby="dateHelpText" />
                        <small id="dateHelpText" className="form-text text-muted">
                            Enter the payment date, or select a value from the calendar.
                        </small>
                    </div>
                    <div className="form-group">
                        <label htmlFor="type">Type</label>
                        <select id="type"
                                name="type"
                                value={this.state.type}
                                onChange={this.handleChange}
                                className={this.getFormControlStyles("type")}>
                            <option value="">Select payment type...</option>
                            <option value="cash">Cash</option>
                            <option value="card">Card</option>
                            <option value="cheque">Cheque</option>
                            <option value="transfer">Bank Transfer</option>
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor="amount">Amount</label>
                        <input type="number"
                               id="amount"
                               name="amount"
                               value={this.state.amount}
                               onChange={this.handleChange}
                               className={this.getFormControlStyles("amount")}
                               aria-describedby="amountHelpText" />
                        <small id="amountHelpText" className="form-text text-muted">
                            Enter the payment amount.
                        </small>
                    </div>
                    <div className="form-group">
                        <label htmlFor="type">Customer</label>
                        <select id="custId"
                                name="custId"
                                value={this.state.custId}
                                onChange={this.handleChange}
                                className={this.getFormControlStyles("custId")} >
                            <option value="0">Select a customer...</option>
                            {
                                customers.map(c => <option key={c.id} value={c.id}>{c.name}</option>)
                            }
                        </select>
                    </div>
                    <div className="row justify-content-end">
                        <div className="col-auto">
                            <input type="submit" onClick={this.onSubmit} disabled={this.props.pending}
                                   className="btn btn-primary" value="Add Payment"/>
                        </div>
                    </div>
                </form>
            </>
        );
    }
}

const mapStateToProps = state => ({
    pending: state.pending,
    error: state.error
});

const mapDispatchToProps = dispatch => {
    return {
        insertPayment: payment => dispatch(insertPayment(payment))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddPayment);