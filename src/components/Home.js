import React from "react";
import {Link} from "react-router-dom";

export const Home = () =>
    <div className="jumbotron">
        <h1 className="display-4">Payment Management System</h1>
        <p className="lead">Welcome to the Payment Management System. Select a function below to manage payments.</p>
        <div className="row mt-5 justify-content-center">
            <div className="col-auto">
                <Link to="/list" className="btn btn-lg btn-primary">View Payments</Link>
            </div>
            <div className="col-auto">
                <Link to="/add" className="btn btn-lg btn-success">Add Payment</Link>
            </div>
            <div className="col-auto">
                <Link to="/about" className="btn btn-lg btn-outline-primary">About</Link>
            </div>
        </div>
    </div>;