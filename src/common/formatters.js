
const currencyFormatter = new Intl.NumberFormat("en-GB", { style: "currency", currency: "GBP" });
const dateFormatter = new Intl.DateTimeFormat("en-GB", { dateStyle: "short", timeStyle: "medium" });

export function formatCurrency(value) {
    return currencyFormatter.format(value);
}

export function formatDate(value) {
    // Formats an incoming date string using the format defined above. The date string needs to be parsed as a date
    // before formatting can be applied.

    return dateFormatter.format(Date.parse(value));
}