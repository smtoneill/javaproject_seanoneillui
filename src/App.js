import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { NavLink, Route, Switch } from "react-router-dom";
import { Home } from "./components/Home";
import About from "./components/About";
import PaymentListPage from "./components/PaymentListPage";
import PaymentDetail from "./components/PaymentDetail";
import AddPayment from "./components/AddPayment";

function App() {
  return (
    <div>
      <nav className="navbar navbar-expand navbar-dark bg-dark">
        <div className="navbar-nav">
          <li className="nav-item">
            <NavLink exact to="/" className="nav-link" activeClassName="active">
              Home
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink to="/list" className="nav-link" activeClassName="active">
              View Payments
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink to="/add" className="nav-link" activeClassName="active">
              Add New Payment
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink to="/about" className="nav-link" activeClassName="active">
              About
            </NavLink>
          </li>
        </div>
      </nav>
      <div className="container my-4">
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/about" component={About} />
          <Route exact path="/list" component={PaymentListPage} />
          <Route exact path="/view/:id" component={PaymentDetail} />
          <Route exact path="/add" component={AddPayment} />
        </Switch>
      </div>
    </div>
  );
}

export default App;
