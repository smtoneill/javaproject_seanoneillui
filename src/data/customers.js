// Normally customer data would come from the API but since this does not exist in the Payment API, I have hard-coded
// some values here...

const customers = [
    { id: 1111, name: "Customer 1" },
    { id: 2222, name: "Customer 2" },
    { id: 3333, name: "Customer 3" },
    { id: 4444, name: "Customer 4" },
    { id: 5555, name: "Customer 5" }
];

export default customers;